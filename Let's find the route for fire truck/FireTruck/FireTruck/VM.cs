﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace FireTruck
{

    class VM : INotifyPropertyChanged
    {
        public const int MAX_STREET = 21;
        public const int FIRE_STATION = 1;
        public static readonly char[] DELIMITER = new char[] { ' ' };
        public BindingList<StreetCase> StreetCases = new BindingList<StreetCase>();
        private string result;
        public string Result { get => result; set { result = value; onChange(); } }
        public void Calculate()
        {
            try
            {
                Result = "";
                int i = 1;
                foreach (StreetCase s in StreetCases)
                {
                    Result += "Case " + (i++) + "\n\r";
                    if (!s.IsDeadLock)
                    {
                        int currentStreet = FIRE_STATION;
                        List<int> solution = new List<int>();
                        s.Solutions.Clear();
                        List<int> sol = getNext(s, solution, currentStreet);
                        ShowResult(s.Solutions);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void ShowResult(List<List<int>> results)
        {
            foreach (List<int> res in results)
            {
                foreach (int r in res)
                    Result += r + "   ";
                Result += "\n\r";
            }
        }
        private List<int> getNext(StreetCase s, List<int> solution, int currentStreet)
        {
            try
            {
                if (solution.Contains(currentStreet))
                {
                    //close path
                }
                else if ((currentStreet != s.FireStreet))
                {
                    solution.Add(currentStreet);
                    int n = s.AllPaths[currentStreet].Count;
                    for (int i = 1; i < n; i++)
                    {
                        List<int> sol = new List<int>(solution);
                        getNext(s, sol, s.AllPaths[currentStreet][i]);
                    }
                }
                else if (currentStreet == s.FireStreet)
                {
                    solution.Add(currentStreet);
                    s.Solutions.Add(solution);
                }
            }
            catch (Exception ex)
            {

            }
            return solution;
        }
        public void Browse()
        {
            try
            {
                bool isNewCase = true;
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.DefaultExt = ".txt";
                fileDialog.Filter = "Text documents (.txt)|*.txt";
                if (fileDialog.ShowDialog() == true)
                {
                    StreetCases.Clear();
                    StreetCase s = new StreetCase();
                    string FileName = Path.GetFileName(fileDialog.FileName);
                    string[] lines = File.ReadAllLines(fileDialog.FileName);
                    foreach (string line in lines)
                    {
                        string[] items = line.Split(DELIMITER, StringSplitOptions.RemoveEmptyEntries);
                        if (isNewCase)
                        {
                            if (items.Length > 1)
                            {
                                // if no fire street defined in new case
                                break;
                            }
                            s.FireStreet = Convert.ToInt32(items[0]);
                            if (s.StreetCount < s.FireStreet)
                                s.StreetCount = s.FireStreet;
                            isNewCase = false;
                        }
                        else if (items.Length == 2)
                        {
                            int street1 = Convert.ToInt32(items[0]);
                            int street2 = Convert.ToInt32(items[1]);
                            if ((street1 > MAX_STREET) || (street2 > MAX_STREET))
                            {
                                // exceeeds the limit of street
                                break;
                            }
                            else if ((street1 > 0) && (street2 > 0))
                            {
                                if (s.StreetCount < street1)
                                    s.StreetCount = street1;
                                if (s.StreetCount < street2)
                                    s.StreetCount = street2;
                                addStreet(s, street1, street2);
                                addStreet(s, street2, street1);

                            }
                            else if ((street1 == 0) && (street2 == 0))
                            {
                                isNewCase = true;
                                StreetCases.Add(s);
                                s = new StreetCase();
                            }
                        }
                        else
                        {
                            //incorrect path
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        private void addStreet(StreetCase s, int street1, int street2)
        {

            if (s.FireStreet == street2)
                s.IsDeadLock = false;
            if (s.AllPaths.Count <= street1)
                s = addDefaultStreet(s, street1);
            s.AllPaths[street1].Add(street2);
        }
        private StreetCase addDefaultStreet(StreetCase streetCase, int street)
        {
            for (int i = streetCase.AllPaths.Count; i <= street; i++)
                streetCase.AllPaths.Add(new List<int> { i });
            return streetCase;
        }
        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void onChange([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
