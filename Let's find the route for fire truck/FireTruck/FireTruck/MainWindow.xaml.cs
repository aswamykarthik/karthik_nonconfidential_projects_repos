﻿using System.Windows;

namespace FireTruck
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }
        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            vm.Browse();
        }

        private void BtnCalculate_Click(object sender, RoutedEventArgs e)
        {
            vm.Calculate();
        }
    }
}
