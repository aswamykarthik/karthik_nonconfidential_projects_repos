﻿using System.Collections.Generic;

namespace FireTruck
{
    public class StreetCase
    {
        const int MAX_STREET = 21;
        public List<List<int>> AllPaths = new List<List<int>>();
        public List<List<int>> Solutions = new List<List<int>>();
        public int FireStreet;
        public int StreetCount = 0;
        public bool IsDeadLock = true;
    }
}
